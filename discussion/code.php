<?php 

// [ SECTION ] Repetition Control Structures

	// It is used to execute code multiple times.
	// While Loop
		// A While loop takes a single condition.

function whileLoop() {
	$count = 5;

	while($count !== 0) {
		echo $count.'<br/>';
		//echo $count;
		$count--;
	}
}

// function whileLoop1($count) {

// 	while($count !== 0) {
// 		echo $count.'<br/>';
// 		$count--;
// 	}
// }

	// Do-While Loop
		// A do-while loop workds a lot like the while loop. The only difference is that, do-while guarantee that the code will run/executed at least once. 

function doWhileLoop() {
	$count = 20;

	do {
		echo $count.'<br/>';
		$count--;
	} while ($count > 20);
}

	// For Loop
		/*
			for (initial value; condition; iteration) {
				// code block
			}
		*/

function forLoop() {
	for($count = 0; $count <= 20; $count++) {
		echo $count.'<br/>';
	}
}


	// Continue and Break Statements
		// "Continue" is a keyword that allows the code to go the next loop without finishing the current code block.
		// "Break" on the other hand is a keyword that stops the execution of the current loop. 

function modifiedForLoop() {
    for ($count = 0; $count <= 20; $count++) {
        // If the count is divisible by 2, continue to the next iteration
        if ($count % 2 === 0) {
            continue;
        }
        // If none of the conditions are met, echo the count
        echo $count . '<br/>';

        // If the count is greater than 10, break the loop
        if ($count > 10) {
            break;
        }
    }
}

// [SECTION] Array Manipulation

	// An array is a lind of variable that can hold more than one value.
	// Arrays are declared using array() function of square brackets [].
		// In the earlier version of php, we cannot use '[]'. but as of Php 5.4, we can use the short array syntax which replace array() with square brackets []

$studentNumbers = array('2020-1923', '2020-1924', '2020-1965', '2020-1926', '2020-1927');
// Before the Php 5.4

$studentNumbers = ['2020-1923', '2020-1924', '2020-1965', '2020-1926', '2020-1927'];
// After the php 5.4

// Simple Arrays
$grades = [98.5, 94.3, 89.2, 90.1];
$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];
$tasks = [
	'drink html',
	'eat php',
	'inhale css',
	'bake javascript'
];


// Associative Array
	// Associate Array differ from numeric array in the sense that associative arrays uses descriptive names in naming the elements/values (key=>value pair).
	// Double Arrow Operator(=>) - an assignment operator that is commonly used in the creation of associative array.

$gradePeriods = ['firstGrading' => 98.5, 'secondGrading' =>94.3, 'thirdGrading' => 89.2, 'fourthGrading' => 90.1];

// Two-Dimentional Array
	// Two-Dimentional Array is commonly used in image processing, good example of this is our viewing scren that uses multidimentional array of pixels.


$heroes = [

	['iron man', 'thor', 'hulk'],
	['wolverine', 'cyclops', 'jean grey'],
	['batman', 'superman', 'wonderwoman']
];

// Two-Dimentional Assocative Array

$ironManPowers = [
	// 'regular' & 'signature' will be the label
	// 'power' is the item inside the label
	'regular' => ['repulsur blast', 'rocket punch'],
	'signature' => ['unibeam']
];


// Array Iterative Method: foreach();
// foreach($heroes as $hero) {
// 	print_r($hero);
// }

// [SECTION] Array Mutators
// Array Mutators modidy the contents of an array. 

// Array Sorting
$sortedBrands = $computerBrands;
$reverseSortedBrands = $computerBrands;

sort($sortedBrands);
rsort($reverseSortedBrands);



?>




















