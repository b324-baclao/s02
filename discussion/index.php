<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S02: Selection Control Structures and Array Manipulation</title>
	</head>
	<body>
		<h1>Repetition Control Structures</h1>

		<h2>While Loop</h2>
		<?php whileLoop(); ?>

		<h2>Do-While Loop</h2>
		<?php doWhileLoop(); ?>

		<h2>For Loop</h2>
		<?php forLoop(); ?>

		<h2>Continue and Break</h2>
		<?php modifiedForLoop(); ?>

		<h1>Array Manipulation</h1>

		<h2>Types of Arrays</h2>

		<h3>Simple Array</h3>

		<ul>
			<!-- php codes/statements can be breakdown using the php tags -->
		    <?php foreach ($computerBrands as $brand) { ?>
		    	<!-- PHP Includes a short hand for "php echo tag" -->
		        <li><?= $brand; ?></li>
		    <?php } ?>
		</ul>

		<h3>Associative Array</h3>

		<ul>
			<?php foreach ($gradePeriods as $period => $grade) { ?>
				<li>
					Grade in <?= $period ?> is <?= $grade ?>
				</li>
			<?php } ?>
		</ul>

		<h3>Multidimentional Array</h3>

		<ul>
			<?php 
				// Each $heroes will be represented by a $team (accessing the outer array elements)
				foreach($heroes as $team){
					// Each $team will be represented by a $member (accessing the inner elements)
					foreach($team as $member){
						?>	
							<li><?php echo $member ?></li>
						<?php
					}
				}
			?>
		</ul>

		<h3>Two-Dimentional Assocate Array</h3>

		<ul>
			<?php
				foreach($ironManPowers as $label => $powerGroup){
					foreach($powerGroup as $power){
						?>
							<li><?= "$label: $power" ?></li>
						<?php
					}
				}
			?>
		</ul>

		<h1>Mutators</h1>
		<h3>Sorting</h3>
		<p><?php print_r($sortedBrands); ?></p>
		<p><?php print_r($reverseSortedBrands); ?></p>

		<h3>Push</h3>
		<!-- Adds one or more elements to the end of an array. -->
		<?php array_push($computerBrands, 'Apple'); ?>
		<p><?php print_r ($computerBrands); ?></p>

		<h3>Unshift</h3>
		<!-- Adds one or more elements to the beginning of an array. -->
		<?php array_unshift($computerBrands, 'Dell'); ?>
		<p><?php print_r ($computerBrands); ?></p>

		<h3>Pop</h3>
		<!-- Removes and returns the last element from an array. -->
		<?php array_pop($computerBrands); ?>
		<p><?php print_r ($computerBrands); ?></p>

		<h3>Shift</h3>
		<!-- Removes and returns the first element from an array. -->
		<?php array_shift($computerBrands); ?>
		<p><?php print_r ($computerBrands); ?></p>

		<h3>Count</h3>
		<p><?php echo count($computerBrands); ?></p>

		<h3>In array</h3>
		<p><?php var_dump(in_array('Acer', $computerBrands)); ?></p>
		

	</body>
</html>