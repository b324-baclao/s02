<?php 


// Acitivity 1

function divisibleByFive() {
    $count = 0; // Initialize a counter to keep track of the number of iterations
    $number = 0; // Start with the first number (0)
    $result = ''; // Initialize an empty string to store the result

    // Use a while loop to continue until we reach the 100th iteration
    while ($count < 100) {
        // Check if the number is divisible by 5
        if ($number % 5 === 0) {
            // Append the number and a comma to the result string
            $result .= $number . ', ';
            $count++; // Increment the counter
        }

        $number++; // Move to the next number
    }

    // Return the result
    return $result;
}


// Activity 2

$students = [];



?>