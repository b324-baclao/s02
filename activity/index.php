<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Activity 2: Selection Control Structures and Array Manipulation</title>
	</head>
	<body>

		<!-- Activity 1: Divisible by 5 -->
		<h2>Divisibles of Five</h2>
		<?php echo divisibleByFive(); ?>

		<!-- Activity 2: Array -->

		<h2>Array Manipulation</h2>

		<p><?php array_push($students, 'John Smith'); ?></p>
		<p>array(1) { [0]=> string(10) "<?php echo $students[0]; ?>" }</p>
		<p><?php echo count($students); ?></p>

		<p><?php array_push($students, 'Jane Smith'); ?></p>
		<p>array(2) { [0]=> string(10) "<?php echo $students[0]; ?>" [1]=> string(10) "<?php echo $students[1]; ?>" }</p>
		<p><?php echo count($students); ?></p>

		<p><?php array_shift($students); ?></p>
		<p>array(1) { [0]=> string(10) "<?php echo $students[0]; ?>" }</p>
		<p><?php echo count($students); ?></p>






	</body>
</html>

